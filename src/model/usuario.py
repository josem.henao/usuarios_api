# Atributos de un Usuario:
#
# id_usuario
# email
# username
# nombre
# apellido
# genero
# fecha_nacimiento
# lugar_nacimiento
# identificacion
# imagen
# grupo


class Usuario():
    def __init__(self, *args, **kwargs):
        self.__id_usuario = kwargs.get('id_usuario')
        self.__email = kwargs.get('email')
        self.__username = kwargs.get('username')
        self.__nombre = kwargs.get('nombre')
        self.__apellido = kwargs.get('apellido')
        self.__genero = kwargs.get('genero')
        self.__fecha_nacimiento = kwargs.get('fecha_nacimiento')
        self.__lugar_nacimiento = kwargs.get('lugar_nacimiento')
        self.__identificacion = kwargs.get('identificacion')
        self.__imagen = kwargs.get('imagen')
        self.__grupos = kwargs.get('grupos')

    def get_id_usuario(self):
        return self.__id_usuario

    def get_email(self):
        return self.__email

    def get_username(self):
        return self.__username

    def get_nombre(self):
        return self.__nombre

    def get_apellido(self):
        return self.__apellido

    def get_genero(self):
        return self.__genero

    def get_fecha_nacimiento(self):
        return self.__fecha_nacimiento

    def get_lugar_nacimiento(self):
        return self.__lugar_nacimiento

    def get_identificacion(self):
        return self.__identificacion

    def get_imagen(self):
        return self.__imagen

    def get_grupos(self):
        return self.__grupos

    def __str__(self):
        return '{}. {}'.format(self.__id_usuario, self.__username)

    def es_de_grupo(self, grupo):
        return grupo in self.__grupos
