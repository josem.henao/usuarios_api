from unittest import TestCase

from src.model.recurso import Recurso


class TestRecurso(TestCase):

    recurso_1 = Recurso(codigo=1,
                        recurso='Crear Torneos',
                        descripcion='Con este recurso se puede crear un '
                                    'Torneo'
                        )
    recurso_2 = Recurso(codigo=2,
                        recurso='Consultar Torneos',
                        descripcion='Con este recurso se puede consultar un '
                                    'Torneo'
                        )

    def test_get_codigo(self):
        # Arrange
        recurso = self.recurso_1
        codigo = 1

        # Act
        result = recurso.get_codigo()

        # Assert
        TestCase.assertEqual(self, codigo, result)

    def test_get_recurso(self):
        # Arrange
        recurso = self.recurso_2
        rec = 'Consultar Torneos'

        # Act
        result = recurso.get_recurso()

        # Assert
        TestCase.assertEqual(self, rec, result)

    def test_get_descripcion(self):
        # Arrange
        recurso = self.recurso_2
        descripcion = 'Con este recurso se puede consultar un Torneo'

        # Act
        result = recurso.get_descripcion()

        # Assert
        TestCase.assertEqual(self, descripcion, result)
