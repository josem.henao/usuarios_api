from unittest import TestCase

from src.model.genero import Genero


class TestGenero(TestCase):

    genero_1 = Genero(codigo=1,
                      genero='Masculino',
                      descripcion='Genero que representa a los Torneos o '
                                  'Equipo donde juegan hombres'
                      )

    def test_get_codigo(self):
        # Arrange
        genero = self.genero_1
        codigo = 1

        # Act
        result = genero.get_codigo()

        # Assert
        TestCase.assertEqual(self, codigo, result)

    def test_get_genero(self):
        # Arrange
        genero = self.genero_1
        gen = 'Masculino'

        # Act
        result = genero.get_genero()

        # Assert
        TestCase.assertEqual(self, gen, result)

    def test_get_descripcion(self):
        # Arrange
        genero = self.genero_1
        descripcion = 'Genero que representa a los Torneos o Equipo donde ' \
                      'juegan hombres'

        # Act
        result = genero.get_descripcion()

        # Assert
        TestCase.assertEqual(self, descripcion, result)
