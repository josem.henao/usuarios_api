from datetime import datetime
from unittest import TestCase

from src.model.genero import Genero
from src.model.rol import Rol
from src.model.recurso import Recurso
from src.model.session import Session
from src.model.usuario import Usuario


class TestSession(TestCase):
    genero_1 = Genero(codigo=1,
                      genero='Masculino',
                      descripcion='Genero Masculino'
                      )
    recurso_1 = Recurso(codigo=1,
                        recurso='Crear Torneos',
                        descripcion='Con este recurso se puede crear un '
                                    'Torneo'
                        )

    grupo_1 = Rol(codigo=1,
                  grupo='Administrador de Torneos',
                  descripcion='Puede administrar por completo un torneo, es '
                                'decir, hacer CRUD de Torneos',
                  recursos=[recurso_1]
                  )

    usuario_1 = Usuario(id_usuario=1202,
                        email='josem.henao@gmail.com',
                        username='josemhenao',
                        nombre='José Miguel',
                        genero=genero_1,
                        fecha_nacimiento=datetime(1994, 6, 22),
                        lugar_nacimiento=655,
                        identificacion='1127594252',
                        grupos=[grupo_1]
                        )

    session_1 = Session(codigo=1,
                        usuario=usuario_1,
                        )

    def test_get_codigo(self):
        # Arrange
        session = self.session_1
        codigo = 1

        # Act
        result = session.get_codigo()

        # Assert
        TestCase.assertEqual(self, codigo, result)

    def test_get_token_no_generado(self):
        # Arrange
        session = Session()

        # Act
        result = session.get_token()

        # Assert
        TestCase.assertIsNone(self, result)

    def test_get_token_generado(self):
        # Arrange
        session = Session()

        # Act
        session.generate_token()
        result = session.get_token()

        # Assert
        TestCase.assertIsNotNone(self, result)

    def test_get_usuario(self):
        # Arrange
        session = self.session_1
        usuario = self.usuario_1

        # Act
        result = session.get_usuario()

        # Assert
        TestCase.assertEqual(self, usuario, result)

    def test_get_usuario_is_None(self):
        # Arrange
        session = Session()

        # Act
        result = session.get_usuario()

        # Assert
        TestCase.assertIsNone(self, result)

    def test_get_fecha_creacion(self):
        # Arrange
        session = self.session_1
        today = datetime.today().date()

        # Act
        created_at_result = session.get_fecha_creacion()
        result = created_at_result.date()
        # Assert
        TestCase.assertEqual(self, today, result)

    def test_get_fecha_creacion_is_not_None(self):
        # Arrange
        session = Session()

        # Act
        result = session.get_fecha_creacion()

        # Assert
        TestCase.assertIsNotNone(self, result)

    def test_generate_token(self):
        # Arrange
        session = Session()

        # Act
        result = session.generate_token()

        # Assert
        TestCase.assertTrue(self, result)

    def test_generate_token_ya_generado(self):
        # Arrange
        session = Session()
        session.generate_token()

        # Act
        result = session.generate_token()

        # Assert
        TestCase.assertFalse(self, result)

    def test_token_con_longitud_correcta(self):
        # Arrange
        session = Session()
        longitud = 60

        # Act
        session.generate_token()
        result = len(session.get_token())

        # Assert
        TestCase.assertEqual(self, longitud, result)
