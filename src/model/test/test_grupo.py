from unittest import TestCase

from src.model.rol import Rol
from src.model.recurso import Recurso


class TestGrupo(TestCase):
    recurso_1 = Recurso(codigo=1,
                        recurso='Crear Torneos',
                        descripcion='Con este recurso se puede crear un '
                                    'Torneo'
                        )
    recurso_2 = Recurso(codigo=2,
                        recurso='Consultar Torneos',
                        descripcion='Con este recurso se puede consultar un '
                                    'Torneo'
                        )
    recurso_3 = Recurso(codigo=3,
                        recurso='Actualizar Torneos',
                        descripcion='Con este recurso se puede actualizar un '
                                    'Torneo'
                        )
    recurso_4 = Recurso(codigo=4,
                        recurso='Eliminar Torneos',
                        descripcion='Con este recurso se puede eliminar un '
                                    'Torneo'
                        )
    grupo_1 = Rol(codigo=1,
                  grupo='Administrador de Torneos',
                  descripcion='Puede administrar por completo un torneo, es '
                                'decir, hacer CRUD de Torneos',
                  recursos=[recurso_1, recurso_2, recurso_3, recurso_4]
                  )

    def test_get_codigo(self):
        # Arrange
        grupo = self.grupo_1
        codigo = 1

        # Act
        result = grupo.get_codigo()

        # Assert
        TestCase.assertEqual(self, codigo, result)

    def test_get_grupo(self):
        # Arrange
        grupo = self.grupo_1
        gr = 'Administrador de Torneos'

        # Act
        result = grupo.get_grupo()

        # Assert
        TestCase.assertEqual(self, gr, result)

    def test_get_descripcion(self):
        # Arrange
        grupo = self.grupo_1
        descripcion = 'Puede administrar por completo un torneo, es ' \
                      'decir, hacer CRUD de Torneos'

        # Act
        result = grupo.get_descripcion()

        # Assert
        TestCase.assertEqual(self, descripcion, result)

    def test_get_recursos(self):
        # Arrange
        grupo = self.grupo_1
        recursos = [self.recurso_1, self.recurso_2, self.recurso_3,
                    self.recurso_4]

        # Act
        result = grupo.get_recursos()

        # Assert
        TestCase.assertEqual(self, recursos, result)

    def test_tiene_recurso(self):
        # Arrange
        grupo = self.grupo_1
        recurso = self.recurso_1

        # Act
        result = grupo.tiene_recurso(recurso)

        # Assert
        TestCase.assertTrue(self, result)

    def test_tiene_recurso_incorrecto(self):
        # Arrange
        grupo = self.grupo_1
        recurso = Recurso(codigo=5,
                          recurso='Eliminar Usuarios',
                          descripcion='Con este recurso se puede eliminar a un'
                                      ' Usuario'
                          )

        # Act
        result = grupo.tiene_recurso(recurso)

        # Assert
        TestCase.assertFalse(self, result)
