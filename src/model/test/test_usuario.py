from unittest import TestCase
from datetime import datetime
from src.model.genero import Genero
from src.model.rol import Rol
from src.model.recurso import Recurso
from src.model.usuario import Usuario


class TestUsuario(TestCase):
    genero_1 = Genero(codigo=1,
                      genero='Masculino',
                      descripcion='Genero Masculino'
                      )
    recurso_1 = Recurso(codigo=1,
                        recurso='Crear Torneos',
                        descripcion='Con este recurso se puede crear un '
                                    'Torneo'
                        )
    recurso_2 = Recurso(codigo=2,
                        recurso='Consultar Torneos',
                        descripcion='Con este recurso se puede consultar un '
                                    'Torneo'
                        )
    recurso_3 = Recurso(codigo=3,
                        recurso='Actualizar Torneos',
                        descripcion='Con este recurso se puede actualizar un '
                                    'Torneo'
                        )
    recurso_4 = Recurso(codigo=4,
                        recurso='Eliminar Torneos',
                        descripcion='Con este recurso se puede eliminar un '
                                    'Torneo'
                        )
    grupo_1 = Rol(codigo=1,
                  grupo='Administrador de Torneos',
                  descripcion='Puede administrar por completo un torneo, es '
                                'decir, hacer CRUD de Torneos',
                  recursos=[recurso_1, recurso_2, recurso_3, recurso_4]
                  )

    grupo_2 = Rol(codigo=2,
                  grupo='Consultar Torneos',
                  descripcion='Puede consultar un torneo, es decir, hacer '
                                'READ a un Torneo',
                  recursos=[recurso_2]
                  )

    usuario_1 = Usuario(id_usuario=1202,
                        email='josem.henao@gmail.com',
                        username='josemhenao',
                        nombre='José Miguel',
                        apellido='Henao Cardona',
                        genero=genero_1,
                        fecha_nacimiento=datetime(1994, 6, 22),
                        lugar_nacimiento=655,
                        identificacion='1127594252',
                        imagen='profile/default.png',
                        grupos=[grupo_1]
                        )

    def test_get_id_usuario(self):
        # Arrange
        usuario = self.usuario_1

        # Act
        result = usuario.get_id_usuario()

        # Assert
        TestCase.assertEqual(self, 1202, result)

    def test_get_email(self):
        # Arrange
        usuario = self.usuario_1

        # Act
        result = usuario.get_email()

        # Assert
        TestCase.assertEqual(self, 'josem.henao@gmail.com', result)

    def test_get_username(self):
        # Arrange
        usuario = self.usuario_1

        # Act
        result = usuario.get_username()

        # Assert
        TestCase.assertEqual(self, 'josemhenao', result)

    def test_get_nombre(self):
        # Arrange
        usuario = self.usuario_1

        # Act
        result = usuario.get_nombre()

        # Assert
        TestCase.assertEqual(self, 'José Miguel', result)

    def test_get_apellido(self):
        # Arrange
        usuario = self.usuario_1

        # Act
        result = usuario.get_apellido()

        # Assert
        TestCase.assertEqual(self, 'Henao Cardona', result)

    def test_get_genero(self):
        # Arrange
        usuario = self.usuario_1

        # Act
        result = usuario.get_genero()

        # Assert
        TestCase.assertEqual(self, self.genero_1, result)

    def test_get_fecha_nacimiento(self):
        # Arrange
        usuario = self.usuario_1
        fecha = datetime(1994, 6, 22)

        # Act
        result = usuario.get_fecha_nacimiento()

        # Assert
        TestCase.assertEqual(self, fecha, result)

    def test_get_lugar_nacimiento(self):
        # Arrange
        usuario = self.usuario_1

        # Act
        result = usuario.get_lugar_nacimiento()

        # Assert
        TestCase.assertEqual(self, 655, result)

    def test_get_imagen(self):
        # Arrange
        usuario = self.usuario_1

        # Act
        result = usuario.get_imagen()

        # Assert
        TestCase.assertEqual(self, 'profile/default.png', result)

    def test_get_grupo(self):
        # Arrange
        usuario = self.usuario_1

        # Act
        result = usuario.get_grupos()

        # Assert
        TestCase.assertEqual(self, [self.grupo_1], result)

    def test_usuario_pertenece_a_grupo_1(self):
        # Arrange
        usuario = self.usuario_1

        # Act
        result = usuario.es_de_grupo(self.grupo_1)

        # Assert
        TestCase.assertTrue(self, result)

    def test_usuario_no_pertenece_a_grupo_1(self):
        # Arrange
        usuario = self.usuario_1

        # Act
        result = usuario.es_de_grupo(self.grupo_2)

        # Assert
        TestCase.assertFalse(self, result)

    def test_usuario_puede_crear_torneos(self):
        # Arrange
        usuario = self.usuario_1
        recurso = self.recurso_1

        # Act
        result = False
        for g in usuario.get_grupos():
            if g.tiene_recurso(recurso):
                result = True
                break

        # Assert
        TestCase.assertTrue(self, result)

    def test_usuario_puede_consultar_torneos(self):
        # Arrange
        usuario = self.usuario_1
        recurso = self.recurso_2

        # Act
        result = False
        for g in usuario.get_grupos():
            if g.tiene_recurso(recurso):
                result = True
                break

        # Assert
        TestCase.assertTrue(self, result)

    def test_usuario_puede_actualizar_torneos(self):
        # Arrange
        usuario = self.usuario_1
        recurso = self.recurso_3

        # Act
        result = False
        for g in usuario.get_grupos():
            if g.tiene_recurso(recurso):
                result = True
                break

        # Assert
        TestCase.assertTrue(self, result)

    def test_usuario_puede_eliminar_torneos(self):
        # Arrange
        usuario = self.usuario_1
        recurso = self.recurso_4

        # Act
        result = False
        for g in usuario.get_grupos():
            if g.tiene_recurso(recurso):
                result = True
                break

        # Assert
        TestCase.assertTrue(self, result)
