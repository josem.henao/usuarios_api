class Genero():
    def __init__(self, *args, **kwargs):
        self.__codigo = kwargs.get('codigo')
        self.__genero = kwargs.get('genero')
        self.__descripcion = kwargs.get('descripcion')

    def get_codigo(self):
        return self.__codigo

    def get_genero(self):
        return self.__genero

    def get_descripcion(self):
        return self.__descripcion

    def __str__(self):
        return self.__genero
