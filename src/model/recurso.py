class Recurso:

    def __init__(self, *args, **kwargs):
        self.__codigo = kwargs.get('codigo')
        self.__recurso = kwargs.get('recurso')
        self.__descripcion = kwargs.get('descripcion')

    def get_codigo(self):
        return self.__codigo

    def get_recurso(self):
        return self.__recurso

    def get_descripcion(self):
        return self.__descripcion

    def __str__(self):
        return self.__recurso
