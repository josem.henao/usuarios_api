from datetime import datetime
from random import randint

chars = ['!', '#', '$', '%', '&', '(', ')', '*', '+', '-', '0', '1', '2', '3',
         '4', '5', '6', '7', '8', '9', '<', '=', '>', '?', '@', 'A', 'B', 'C',
         'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
         'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', '^', 'a', 'b', 'c',
         'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
         'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']


class Session:
    def __init__(self, *args, **kwargs):
        self.__codigo = kwargs.get('codigo')
        self.__token = kwargs.get('token') or None
        self.__usuario = kwargs.get('usuario')
        self.__fecha_creacion = datetime.now()

    def get_codigo(self):
        return self.__codigo

    def get_token(self):
        return self.__token

    def get_usuario(self):
        return self.__usuario

    def get_fecha_creacion(self):
        return self.__fecha_creacion

    def __str__(self):
        return self.__token if self.__token is not None else "None"

    def generate_token(self):
        if self.__token is None:
            self.__token = ""
            for i in range(60):
                self.__token += chars[randint(0, len(chars) - 1)]
            return True
        else:
            return False


# s = Session()
# print(s)
# print(s.generate_token())
# print(s.get_token())
# print(s.get_usuario())
