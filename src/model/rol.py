class Rol:
    def __init__(self, *args, **kwargs):
        self.__codigo = kwargs.get('codigo')
        self.__rol = kwargs.get('grupo')
        self.__descripcion = kwargs.get('descripcion')
        self.__recursos = kwargs.get('recursos')

    def get_codigo(self):
        return self.__codigo

    def get_grupo(self):
        return self.__rol

    def get_descripcion(self):
        return self.__descripcion

    def get_recursos(self):
        return self.__recursos

    def __str__(self):
        return self.__rol or 'None'

    def tiene_recurso(self, recurso):
        return recurso in self.__recursos
