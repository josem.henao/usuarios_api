class UsuarioException(Exception):
    def __init__(self, *args, **kwargs):
        self._codigo = kwargs.get('codigo') or ''
        self._nombre = kwargs.get('nombre') or ''
        self._mensaje = kwargs.get('mensaje') or ''

    def get_attrs(self):
        return {'codigo': self._codigo,
                'nombre': self._nombre,
                'mensaje': self._mensaje
                }
