from marshmallow import Schema, fields

class UsuarioSchema(Schema):
    id = fields.Number()
    identificacion = fields.Str()
    nombre = fields.Str()
    apellido = fields.Str()
    email = fields.Str()
    password = fields.Str()
    created_at = fields.DateTime()
    updated_at = fields.DateTime()
