# coding=utf-8
from sqlalchemy.exc import IntegrityError
from entities.entity import Session, engine, Base
from entities.usuarioEntity import UsuarioEntity

# generate database schema
Base.metadata.create_all(engine)

# start session
session = Session()

# check for existing data
usuarios = session.query(UsuarioEntity).all()

try:
    # create and persist dummy user
    usuario_1 = UsuarioEntity("1036345878", "Juan Andrés", "Farías Duque", "juan.farias@gmail.com", "juan.farias123")
    session.add(usuario_1)
    session.commit()
except IntegrityError:
    print("Se está violando una regla de integridad de los datos")
finally:
    session.close()

if len(usuarios) == 0:
    # create and persist dummy exam
    usuario_1 = UsuarioEntity("1036345878", "Juan Andrés", "Farías Duque", "juan.farias@gmail.com", "juan.farias123")
    session.add(usuario_1)
    session.commit()
    session.close()

    # reload exams
    usuarios = session.query(UsuarioEntity).all()

# show existing exams
print('### Usuarios:')
for user in usuarios:
    print(f'({user.id}) {user.nombre} - {user.email}')