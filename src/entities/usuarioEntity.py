from sqlalchemy import Column, String

from .entity import Entity, Base


class UsuarioEntity(Entity, Base):
    __tablename__ = 'usuarios'

    identificacion = Column(String(32), unique=True)
    nombre = Column(String(50))
    apellido = Column(String(50))
    email = Column(String(50), unique= True)
    password = Column(String(256))

    def __init__(self, identificacion, nombre, apellido, email, password):
        Entity.__init__(self)
        self.identificacion = identificacion
        self.nombre = nombre
        self.apellido = apellido
        self.email = email
        self.password = password
