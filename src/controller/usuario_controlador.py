from abc import ABC

from flask import Flask, jsonify
from flask_cors import CORS

app = Flask(__name__)
CORS(app)
app.debug = True

class UsuarioControlador(ABC):

    @staticmethod
    @app.route('/')
    @app.route('/home')
    def home_GET():
        return jsonify({'mensaje': 'Hola Pilae Usuarios!'})

    @staticmethod
    @app.route('/usuario/<int:id_usuario>', methods=['GET'])
    def usuario_get(*args, **kwargs):
        return jsonify({
            'mensaje':'Se esta consultando al usuario con id: {}'.format(kwargs.get('id_usuario'))
        })

    @staticmethod
    @app.route('/usuario', methods=['POST'])
    def usuario_post(*args, **kwargs):
        return jsonify({
            'mensaje': 'Se esta haciendo un POST de usuario'
        })
